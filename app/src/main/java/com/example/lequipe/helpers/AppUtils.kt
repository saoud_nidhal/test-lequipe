package com.example.lequipe.helpers

import com.example.lequipe.DataConstants.HEIGHT_PICTURE_PARAMS
import com.example.lequipe.DataConstants.WIDTH_PICTURE_PARAMS

object AppUtils {
    fun getLinkOfPictureFromImageSize(link: String, pictureWidth: Int, pictureHeigth: Int): String {
        return link.replace(WIDTH_PICTURE_PARAMS, pictureWidth.toString(), true)
            .replace(HEIGHT_PICTURE_PARAMS, pictureHeigth.toString(), true)
    }
}