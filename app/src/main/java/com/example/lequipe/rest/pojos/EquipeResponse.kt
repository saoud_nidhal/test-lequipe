package com.example.lequipe.rest.pojos


data class EquipeResponse(val onglets: List<Onglet>)

data class Onglet(
    val flux: Flux,
    override val titre: String
) : GenericResponse

data class Flux(
    val items: List<Item>
)
