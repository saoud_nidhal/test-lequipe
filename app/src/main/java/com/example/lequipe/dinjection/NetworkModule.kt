package com.example.lequipe.dinjection

import android.content.Context
import com.example.lequipe.BuildConfig
import com.example.lequipe.DataConstants
import com.example.lequipe.dinjection.scopes.PerApplication
import com.example.lequipe.rest.EquipeApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

private const val MAX_NUMBER_OF_CALL_ONPARALLAL = 6
private const val TIMOUT_IN_SECONDES: Long = 60

@Module
class NetworkModule {
    @Provides
    @PerApplication
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
            .setLenient()
            .create()
        return gsonBuilder
    }

    @Provides
    @PerApplication
    internal fun provideConverterFactory(gson: Gson): Converter.Factory {
        return GsonConverterFactory.create(gson)

    }

    @Provides
    @PerApplication
    internal fun provideHttpLogger(context: Context): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return logging
    }

    @Provides
    @PerApplication
    internal fun provideDispatcher(): Dispatcher {
        val exec = ThreadPoolExecutor(
            MAX_NUMBER_OF_CALL_ONPARALLAL,
            MAX_NUMBER_OF_CALL_ONPARALLAL,
            1,
            TimeUnit.HOURS,
            LinkedBlockingQueue()
        )
        val dispatcher = Dispatcher(exec)
        dispatcher.maxRequestsPerHost = MAX_NUMBER_OF_CALL_ONPARALLAL
        return dispatcher
    }

    @Provides
    @PerApplication
    internal fun provideOkHttpWithoutAuth(
        context: Context,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        dispatcher: Dispatcher
    ): OkHttpClient {
        var okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(TIMOUT_IN_SECONDES, TimeUnit.SECONDS)
            .readTimeout(TIMOUT_IN_SECONDES, TimeUnit.SECONDS)
            .dispatcher(dispatcher)
        return okHttpClient.build()
    }

    @Provides
    @PerApplication
    internal fun provideRetrofitWithoutAuth(
        converterFactory: Converter.Factory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        var retrofit = Retrofit.Builder()
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl(DataConstants.BASE_URL)
            .client(okHttpClient)
        return retrofit.build()
    }

    @Provides
    @PerApplication
    fun provideEquipeApiController(
        retorfit: Retrofit
    ): EquipeApi {
        return retorfit.create(EquipeApi::class.java)
    }
}