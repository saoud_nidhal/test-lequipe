package com.example.lequipe.main

import com.example.lequipe.rest.EquipeApi
import com.example.lequipe.rest.pojos.EquipeResponse
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainPresenter @Inject constructor(private val mainView: MainContract.View, private val equipeApi: EquipeApi) :
    MainContract.Presenter {


    override fun start() {
        getVideoFlux()
    }

    override fun stop() {

    }

    override fun getVideoFlux() {
        mainView.showLoading()
        GlobalScope.launch(Dispatchers.Default, CoroutineStart.DEFAULT) {
            try {
                successGetVideo(equipeApi.getFluxVideos().await())
            } catch (e: Exception) {
                e.printStackTrace()
                showError()
                GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
                }
            }
        }
    }

    private fun successGetVideo(response: EquipeResponse) {
        GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
            mainView.hideLoading()
            mainView.displayVideoFlux(response)
        }
    }

    private fun showError() {
        GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
            mainView.hideLoading()
            mainView.showError()
        }
    }
}