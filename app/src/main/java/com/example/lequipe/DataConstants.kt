package com.example.lequipe

object DataConstants {

    const val BASE_URL = "https://raw.githubusercontent.com/pedrocactus/Lequipetest/master/"
    const val WIDTH_PICTURE_PARAMS = "{width}"
    const val HEIGHT_PICTURE_PARAMS = "{height}"
}