package com.example.lequipe.main.adapters.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.lequipe.R
import com.example.lequipe.helpers.AppUtils
import com.example.lequipe.rest.pojos.Item


class VideoViewholder(view: View) : RecyclerView.ViewHolder(view) {
    fun onBindViewHolder(item: Item) {
        val mImageOfItem = itemView.findViewById<ImageView>(R.id.picture_item_video_iv)
        val mTextTitle = itemView.findViewById<TextView>(R.id.title_item_video_tv)
        val mTextSport = itemView.findViewById<TextView>(R.id.sport_item_video_tv)
        item.objet.titre?.let {
            mTextTitle.text = it
        } ?: run {
            mTextTitle.text = ""
        }
        item.objet.sport?.let {
            mTextSport.text = it.nom
        } ?: run {
            mTextSport.text = ""
        }
        item.objet.image?.let {
            val res = itemView.context.resources
            Glide.with(itemView.context)
                .load(
                    AppUtils.getLinkOfPictureFromImageSize(
                        it.url,
                        res.getDimensionPixelSize(R.dimen.width_of_videopicture),
                        res.getDimensionPixelSize(R.dimen.height_of_video_picture)
                    )
                )
                .placeholder(R.drawable.logo_euipe)
                .into(mImageOfItem)
        } ?: run {
            mImageOfItem.setImageResource(R.drawable.logo_euipe)
        }
    }
}