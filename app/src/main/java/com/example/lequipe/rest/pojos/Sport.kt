package com.example.lequipe.rest.pojos

import android.os.Parcel
import android.os.Parcelable

data class Sport(
    val nom: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(nom)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Sport> = object : Parcelable.Creator<Sport> {
            override fun createFromParcel(source: Parcel): Sport = Sport(source)
            override fun newArray(size: Int): Array<Sport?> = arrayOfNulls(size)
        }
    }
}