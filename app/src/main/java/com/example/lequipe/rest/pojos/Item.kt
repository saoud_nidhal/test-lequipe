package com.example.lequipe.rest.pojos

import android.os.Parcel
import android.os.Parcelable

data class Item(
    val objet: ObjectInformation
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readParcelable<ObjectInformation>(ObjectInformation::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(objet, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Item> = object : Parcelable.Creator<Item> {
            override fun createFromParcel(source: Parcel): Item = Item(source)
            override fun newArray(size: Int): Array<Item?> = arrayOfNulls(size)
        }
    }
}