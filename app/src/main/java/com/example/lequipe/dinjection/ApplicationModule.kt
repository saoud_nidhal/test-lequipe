package com.example.lequipe.dinjection

import android.app.Application
import android.content.Context
import com.example.lequipe.dinjection.scopes.PerApplication

import dagger.Module
import dagger.Provides


@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }
}
