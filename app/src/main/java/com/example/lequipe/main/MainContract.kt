package com.example.lequipe.main

import com.example.lequipe.base.BasePresenter
import com.example.lequipe.base.BaseView
import com.example.lequipe.rest.pojos.EquipeResponse

class MainContract {
    interface View : BaseView<Presenter> {
        fun showError()
        fun showLoading()
        fun hideLoading()
        fun displayVideoFlux(response: EquipeResponse)
    }
    interface Presenter : BasePresenter {
        fun getVideoFlux()
    }
}