package com.example.lequipe.main.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class SectionsPagerAdapter(
    fm: FragmentManager,
    private val mListOfFragment: List<Pair<Fragment, String>>
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return mListOfFragment[position].first
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return return mListOfFragment[position].second
    }

    override fun getCount(): Int {
        return mListOfFragment.size
    }
}