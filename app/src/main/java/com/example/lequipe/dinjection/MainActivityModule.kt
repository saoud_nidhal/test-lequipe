package com.example.lequipe.dinjection


import com.example.lequipe.dinjection.scopes.PerActivity
import com.example.lequipe.main.MainContract
import com.example.lequipe.main.MainPresenter
import com.example.lequipe.main.ui.MainActivity
import com.example.lequipe.rest.EquipeApi
import dagger.Module
import dagger.Provides

/**
 * Created by bforbank03 on 27/09/2018.
 */
@Module
open class MainActivityModule {

    @PerActivity
    @Provides
    internal fun provideMainView(activity: MainActivity): MainContract.View {
        return activity
    }

    @PerActivity
    @Provides
    internal fun provideMainPresenter(mainView: MainContract.View, equipeApi : EquipeApi):
            MainContract.Presenter {
        return MainPresenter(mainView, equipeApi)
    }
}