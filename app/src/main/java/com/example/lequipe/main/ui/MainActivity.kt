package com.example.lequipe.main.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.lequipe.R
import com.example.lequipe.main.MainContract
import com.example.lequipe.main.adapters.SectionsPagerAdapter
import com.example.lequipe.rest.pojos.EquipeResponse
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {


    private lateinit var sectionsPagerAdapter: SectionsPagerAdapter
    private lateinit var mListOfFragment: MutableList<Pair<Fragment, String>>

    @Inject
    lateinit var mainPresenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        initDataView()
        initView()
    }

    private fun initDataView() {
        mListOfFragment = mutableListOf()
        sectionsPagerAdapter =
            SectionsPagerAdapter(supportFragmentManager, mListOfFragment)
        this.mainPresenter.getVideoFlux()
    }

    private fun initView() {
        view_pager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(view_pager)
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        this.mainPresenter = presenter
    }

    override fun showError() {
        Toast.makeText(this@MainActivity, getString(R.string.erreur), Toast.LENGTH_LONG).show()
    }

    override fun displayVideoFlux(response: EquipeResponse) {
        if (mListOfFragment.isNotEmpty())
            mListOfFragment.clear()
        response.onglets.forEach {
            mListOfFragment.add(Pair(VideosFragment.newInstance(it.flux.items), it.titre))
        }
        sectionsPagerAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        main_probressbar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        main_probressbar.visibility = View.GONE
    }
}