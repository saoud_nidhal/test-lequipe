package com.example.lequipe.rest.pojos

import android.os.Parcel
import android.os.Parcelable

data class ObjectInformation(
    val date_publication: String,
    val image: Image?,
    val sport: Sport?,
    override val titre: String?
) : GenericResponse, Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readParcelable<Image>(Image::class.java.classLoader),
        source.readParcelable<Sport>(Sport::class.java.classLoader),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(date_publication)
        writeParcelable(image, 0)
        writeParcelable(sport, 0)
        writeString(titre)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ObjectInformation> = object : Parcelable.Creator<ObjectInformation> {
            override fun createFromParcel(source: Parcel): ObjectInformation = ObjectInformation(source)
            override fun newArray(size: Int): Array<ObjectInformation?> = arrayOfNulls(size)
        }
    }
}