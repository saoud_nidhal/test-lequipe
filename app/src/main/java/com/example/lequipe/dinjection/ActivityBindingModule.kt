package com.example.lequipe.dinjection

import com.example.lequipe.dinjection.scopes.PerActivity
import com.example.lequipe.main.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun bindMainActivity(): MainActivity


}
