package com.example.lequipe.rest

import com.example.lequipe.rest.pojos.EquipeResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface EquipeApi {

    @GET("video-flux.json")
    fun getFluxVideos() : Deferred<EquipeResponse>

}