package com.example.lequipe.dinjection.compenents

import android.app.Application
import com.example.lequipe.EquipeApplication
import com.example.lequipe.dinjection.ActivityBindingModule
import com.example.lequipe.dinjection.ApplicationModule
import com.example.lequipe.dinjection.MainActivityModule
import com.example.lequipe.dinjection.NetworkModule
import com.example.lequipe.dinjection.scopes.PerApplication

import dagger.BindsInstance
import dagger.Component

@PerApplication
@Component(modules = [ActivityBindingModule::class, ApplicationModule::class, NetworkModule::class, MainActivityModule::class])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
    fun inject(app: EquipeApplication)
}
