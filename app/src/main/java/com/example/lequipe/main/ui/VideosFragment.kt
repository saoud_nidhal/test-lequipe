package com.example.lequipe.main.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lequipe.R
import com.example.lequipe.main.adapters.VideosAdapter
import com.example.lequipe.rest.pojos.Item
import kotlinx.android.synthetic.main.fragment_video.*

private const val BUNDLE_ITEMS_NAME = "BUNDLE_ITEMS_VIDEO_FRAGMENT"

class VideosFragment : Fragment() {

    private lateinit var mAdapterVideo: VideosAdapter
    private lateinit var mListOfVideo: MutableList<Item>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_video, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initView()
    }

    private fun initData() {
        mListOfVideo = mutableListOf()
        arguments?.let {
            mListOfVideo.addAll(it.getParcelableArrayList(BUNDLE_ITEMS_NAME))
        }

        mAdapterVideo = VideosAdapter(mListOfVideo)
    }

    private fun initView() {
        video_main_rv.apply {
            layoutManager = LinearLayoutManager(activity!!)
            setHasFixedSize(false)
            adapter = mAdapterVideo
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(mLitOfItem: List<Item>) =
            VideosFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(BUNDLE_ITEMS_NAME, ArrayList(mLitOfItem))
                }
            }
    }
}